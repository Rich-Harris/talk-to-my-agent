import regexes from 'uap-core/regexes.yaml';

function first ( parsers, userAgent, fallback ) {
	for ( let i = 0; i < parsers.length; i += 1 ) {
		const result = parsers[i]( userAgent );
		if ( result ) return result;
	}

	return fallback;
}

function replaceMatches ( str, m ) {
	return str.replace( /\$(\d)/g, ( tmp, i ) => {
		return m[i] || '';
	}).trim();
}

const user_agent_parsers = regexes.user_agent_parsers.map( parser => {
	const regex = new RegExp( parser.regex );

	return userAgent => {
		const match = regex.exec( userAgent );
		if ( !match ) return null;

		return {
			family: parser.family_replacement ? parser.family_replacement.replace('$1', match[1]) : match[1],
			major: parser.v1_replacement || match[2] || null,
			minor: parser.v2_replacement || match[3] || null,
			patch: parser.v3_replacement || match[4] || null
		};
	};
});

const device_parsers = regexes.device_parsers.map( parser => {
	const regex = new RegExp( parser.regex );

	return userAgent => {
		const match = regex.exec( userAgent );
		if ( !match ) return null;

		return {
			family: (parser.device_replacement ? replaceMatches(parser.device_replacement, match) : match[1]) || 'Other',
			brand:  (parser.brand_replacement  ? replaceMatches(parser.brand_replacement, match)  : null) || null,
			model:  (parser.model_replacement  ? replaceMatches(parser.model_replacement, match)  : match[1]) || null
		};
	};
});

const os_parsers = regexes.os_parsers.map( parser => {
	const regex = new RegExp( parser.regex );

	return userAgent => {
		const match = regex.exec( userAgent );
		if ( !match ) return null;

		return {
			family: parser.os_replacement ? parser.os_replacement.replace('$1', match[1]) : match[1],
			major: parser.os_v1_replacement || match[2] || null,
			minor: parser.os_v2_replacement || match[3] || null,
			patch: parser.os_v3_replacement || match[4] || null,
			patchMinor: parser.os_v4_replacement || match[5] || null
		};
	};
});

const defaultBrowser = {
	family: 'Other',
	major: null,
	minor: null,
	patch: null
};

const defaultDevice = {
	family: 'Other',
	brand: null,
	model: null
};

const defaultOs = {
	family: 'Other',
	major: null,
	minor: null,
	patch: null,
	patchMinor: null
};

export default function parse ( userAgent = navigator.userAgent ) {
	return {
		userAgent,
		browser: first( user_agent_parsers, userAgent, defaultBrowser ),
		device: first( device_parsers, userAgent, defaultDevice ),
		os: first( os_parsers, userAgent, defaultOs )
	};
}
