require( 'source-map-support' ).install();

const assert = require( 'assert' );
const parse = require( '..' );

const UA = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36';

describe( 'talk-to-my-agent', () => {
	it( 'parses a user agent string', () => {
		assert.deepEqual( parse( UA ), {
			userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',
			browser: { family: 'Chrome', major: '50', minor: '0', patch: '2661' },
			device: { family: 'Other', brand: null, model: null },
			os: {
				family: 'Mac OS X',
				major: '10',
				minor: '11',
				patch: '2',
				patchMinor: null
			}
		})
	});
});
