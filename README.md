# talk-to-my-agent

Because all the other user agent parsers I found were a bloody nightmare to set up and use, especially in the browser

## Installation and usage

```bash
npm i talk-to-my-agent
```

```js
import parse from 'talk-to-my-agent';
const { browser, os, device } = parse();
```

## License

MIT
