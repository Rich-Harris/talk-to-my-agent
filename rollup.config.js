import yaml from 'rollup-plugin-yaml';
import buble from 'rollup-plugin-buble';
import nodeResolve from 'rollup-plugin-node-resolve';

export default {
	entry: 'src/index.js',
	plugins: [
		nodeResolve(),
		yaml(),
		buble()
	],
	targets: [
		{ format: 'cjs', dest: 'dist/talk-to-my-agent.cjs.js' },
		{ format: 'es',  dest: 'dist/talk-to-my-agent.es.js' }
	]
};
